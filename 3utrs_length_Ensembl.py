
#delet the first column
awk '{$1="";print $0}' Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3.txt > Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.txt

#delete the whitespace left at the frontmost after deleting the first column
column -t Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.txt > Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.2.txt

#add RR before the second column
awk '{$2="RR "$2; print}' Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.2.txt > Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.2.format.txt




#calculate length of 3'UTR according to the 3'UTR start and 3'UTR end
file_out = open('/Users/siqingl/Work/simon/3pUTR/3utr_length_ensembl/3utrs_length_ensembl_output.txt', 'w')
with open ('/Users/siqingl/Work/simon/3pUTR/3utr_length_ensembl/Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.2.format.txt', 'r') as searchfile:
    for s in searchfile.readlines():
    	str1 = s.find('ENST')
    	str2 = s.find('RR')
    	start = s[0:str2-1]
    	end = s[str2+3:str1-1]
    	start_int = int(start)
    	end_int = int(end)
    	length = end_int - start_int + 1
    	print (length)
    	file_out.write('{}'.format(length)+'\n')       #can instead of by code: file_out.write('%s' % length +'\n')                
file_out.close() 



#the problem is that coordinates of 3'UTRs from "Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.2.format.txt" are franments of each 3'UTR, not the complete region of each 3'UTR




#calculate length of 3'UTR according to the 3'UTR start and 3'UTR end, and add ENS ID in output file
file_out = open('/Users/siqingl/Work/simon/3pUTR/3utr_length_ensembl/3utrs_length_ensembl_output.txt', 'w')
with open ('/Users/siqingl/Work/simon/3pUTR/3utr_length_ensembl/Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.tramV3_delete1column.2.format.txt', 'r') as searchfile:
    for s in searchfile.readlines():
        str1 = s.find('ENST')
        str2 = s.find('RR')
        ENS = s[str1:str1+15]
        start = s[0:str2-1]
        end = s[str2+3:str1-1]
        start_int = int(start)
        end_int = int(end)
        length = end_int - start_int + 1
        print (length)
        file_out.write('{}'.format(length)+' '+ENS+'\n')       #can instead of by code: file_out.write('%s' % length +'\n')   ENS = s[str1:str1+15],here "ENS" cannot be "ID"             
file_out.close() 

