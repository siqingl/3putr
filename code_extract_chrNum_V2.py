#use ID in ensembl_hsa_3utrs.uniq.2ndcolumn.txt to find the line that contain the ID, and write a new text containing chrNum+3'UTR start+3'UTR end+ID 
fwrite = open('/Users/siqingl/Work/yafei/GenomeDownload/3utr_coordinates/ensembl_hsa_3utrs.uniq.2ndcolumn_plus_chrNum.gff3','w')
with open ('/Users/siqingl/Work/yafei/GenomeDownload/Ensembl_3utr_Homo_sapiens.GRCh38.94.chr.gff3', 'r') as searchfile:
    for s in searchfile.readlines():
        with open ('/Users/siqingl/Work/yafei/GenomeDownload/ensembl_hsa_3utrs.uniq.2ndcolumn.txt', 'r') as in_file:
            for f in in_file.readlines():
                ind = s.find(f)
                if ind > 0:
                    chrNum = s[0:ind+15]
                    fwrite.write(chrNum+'\n')                   
fwrite.close() 